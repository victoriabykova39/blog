class Comment < ActiveRecord::Base
  belongs_to :post
  
  validates_presence_of :post_id
  validates_presence_of :body

  scope :persisted, -> { where("id > 0") }
  default_scope -> { order("created_at DESC") }
end
